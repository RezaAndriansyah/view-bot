FROM ubuntu:focal-20220801 AS builder

RUN apt update
RUN apt upgrade -y
RUN apt install wget -y

RUN wget https://go.dev/dl/go1.19.linux-amd64.tar.gz
RUN rm -rf /usr/local/go && tar -C /usr/local -xzf go1.19.linux-amd64.tar.gz

ENV PATH=$PATH:/usr/local/go/bin
ENV PKG_PATH=$GOPATH/src/view-bot

WORKDIR $PKG_PATH/
COPY . $PKG_PATH/

RUN go mod vendor

RUN go run github.com/playwright-community/playwright-go/cmd/playwright@latest install --with-deps

RUN go build main.go

ENTRYPOINT ["./main"]