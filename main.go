package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/RezaAndriansyah/view-bot/models"
	"gitlab.com/RezaAndriansyah/view-bot/pkg/utils"
	"gitlab.com/RezaAndriansyah/view-bot/usecase"
)

var statusRunning = false

func main() {
	gin.SetMode(gin.ReleaseMode)
	r := gin.Default()

	v1 := r.Group("/v1")
	{

		v1.GET("/check/idn", func(ctx *gin.Context) {

			ctx.JSON(
				http.StatusOK,
				utils.ResponseObject{
					Message: "Success checking status idn [handler /check/idn]",
					Data:    statusRunning,
				},
			)

		})

		v1.POST("/view/proxy/idn", func(ctx *gin.Context) {

			var payload models.IDNTimePortalRequest
			body, _ := ioutil.ReadAll(ctx.Request.Body)
			json.Unmarshal(body, &payload)

			statusRunning = true

			err := usecase.IDNTimesPortaWithProxylUsecase(ctx.Request.Context(), payload.URL, payload.Target, payload.TotalTab, payload.TargetRefresh)
			if err != nil {
				statusRunning = false
				ctx.JSON(
					http.StatusInternalServerError,
					utils.ResponseObject{
						Message: "Have error in server [handler /view/idn]",
						Data:    err,
					},
				)
				return
			}

			statusRunning = false
			ctx.JSON(
				http.StatusOK,
				utils.ResponseObject{
					Message: "Success running view bot [handler /view/idn]",
					Data:    nil,
				},
			)

		})

		v1.POST("/view/idn", func(ctx *gin.Context) {

			var payload models.IDNTimePortalRequest
			body, _ := ioutil.ReadAll(ctx.Request.Body)
			json.Unmarshal(body, &payload)

			statusRunning = true

			err := usecase.IDNTimesPortalUsecase(payload.URL, payload.Target, payload.TotalTab, payload.TargetRefresh)
			if err != nil {
				statusRunning = false
				ctx.JSON(
					http.StatusInternalServerError,
					utils.ResponseObject{
						Message: "Have error in server [handler /view/idn]",
						Data:    err,
					},
				)
				return
			}

			statusRunning = false

			ctx.JSON(
				http.StatusOK,
				utils.ResponseObject{
					Message: "Success running view bot [handler /view/idn]",
					Data:    nil,
				},
			)

		})

	}

	fmt.Println("Running in port 7777")
	r.Run(":7777")
}
