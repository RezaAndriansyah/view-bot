package models

type IDNTimePortalRequest struct {
	URL           []string `json:"url"`
	Target        int      `json:"target"`
	TotalTab      int      `json:"total_tab"`
	TargetRefresh int      `json:"target_refresh"`
}

type ResultDataNetPort struct {
	Data    []string `json:"data"`
	Message string   `json:"message"`
}
