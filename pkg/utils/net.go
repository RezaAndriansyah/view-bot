package utils

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
)

func HelperNetPost(url string, jsonStr []byte) (res string, err error) {

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	if err != nil {
		return res, err
	}
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return res, err
	}
	defer resp.Body.Close()

	fmt.Println("Response Status:", resp.Status)
	body, _ := ioutil.ReadAll(resp.Body)

	res = string(body)

	return res, err

}
