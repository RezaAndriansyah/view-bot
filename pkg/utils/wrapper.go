package utils

type ResponseObject struct {
	Data    interface{} `json:"data"`
	Message string      `json:"message"`
}
