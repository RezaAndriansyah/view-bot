package usecase

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"sync"

	"github.com/playwright-community/playwright-go"
	"gitlab.com/RezaAndriansyah/view-bot/models"
	"gitlab.com/RezaAndriansyah/view-bot/pkg/utils"
)

var base_url string = "http://103.152.118.155:6666/v1/rotate"

func IDNTimesPortaWithProxylUsecase(ctx context.Context, listLink []string, target, totalTab, targetRefresh int) (err error) {
	pw, err := playwright.Run()
	if err != nil {
		log.Fatalf("could not launch playwright: %v", err)
		return err
	}

	//target = 10 000

	var totalRefreshForEachPage int = targetRefresh
	var proxyIP []string
	var resultData models.ResultDataNetPort = models.ResultDataNetPort{}

	utils.HelperNetPost(fmt.Sprintf("%v/up", base_url), []byte(fmt.Sprintf(`{"total_service":%v}`, totalTab)))

	for _, itemURL := range listLink {

		limitRuntime := totalTab * totalRefreshForEachPage // 35*(totalRefreshForEachPage)
		totalRuntime := target / limitRuntime              // 1000

		for x := 0; x < totalRuntime; x++ {
			// select {
			// case <-ctx.Done():
			// 	return
			// default:
			// }
			resNet, _ := utils.HelperNetPost(fmt.Sprintf("%v/port", base_url), []byte(fmt.Sprintf(`{"total_service":%v}`, totalTab)))
			json.Unmarshal([]byte(resNet), &resultData)
			proxyIP = append(proxyIP, resultData.Data...)
			IDNRunningRuntimeWithProxy(ctx, pw, x, totalTab, totalRefreshForEachPage, itemURL, proxyIP)
			utils.HelperNetPost(fmt.Sprintf("%v/restart", base_url), []byte(""))
		}

	}

	if err = pw.Stop(); err != nil {
		log.Fatalf("could not stop Playwright: %v", err)
		return err
	}

	utils.HelperNetPost(fmt.Sprintf("%v/down", base_url), []byte(""))

	return err
}

func IDNRunningRuntimeWithProxy(ctx context.Context, pw *playwright.Playwright, x, totalTab, totalRefreshForEachPage int, itemURL string, proxy []string) (err error) {
	fmt.Printf("Runtime %v\n", x+1)

	browser, err := pw.Chromium.Launch()
	if err != nil {
		log.Fatalf("could not launch Chromium: %v", err)
		return err
	}

	contextBrowser, err := browser.NewContext()
	if err != nil {
		log.Fatalf("could not launch Chromium: %v", err)
		return err
	}

	waitGroup := sync.WaitGroup{}

	for i := 0; i < totalTab; i++ {

		fmt.Println(proxy[i])

		page, err := contextBrowser.NewPage(playwright.BrowserNewPageOptions{
			Proxy: &playwright.BrowserNewPageOptionsProxy{
				Server: playwright.String(proxy[i]),
			},
		})
		if err != nil {
			log.Fatalf("could not new page: %v", err)
		}

		waitGroup.Add(1)

		go func(ctx context.Context, page playwright.Page) {

			defer waitGroup.Done()

			for j := 0; j < totalRefreshForEachPage; j++ {
				// select {
				// case <-ctx.Done():
				// 	return
				// default:
				// }
				_, err := page.Goto(itemURL, playwright.PageGotoOptions{
					WaitUntil: playwright.WaitUntilStateDomcontentloaded,
					Timeout:   playwright.Float(60000),
				})
				if err != nil {
					fmt.Println(err)
				}
			}

		}(ctx, page)

	}

	waitGroup.Wait()
	contextBrowser.ClearCookies()

	if err = contextBrowser.Close(); err != nil {
		log.Fatalf("could not close context: %v", err)
		return err
	}

	if err = browser.Close(); err != nil {
		log.Fatalf("could not close browser: %v", err)
		return err
	}

	return err

}

func IDNTimesPortalUsecase(listLink []string, target, totalTab, targetRefresh int) (err error) {
	pw, err := playwright.Run()
	if err != nil {
		log.Fatalf("could not launch playwright: %v", err)
		return err
	}

	var totalRefreshForEachPage int = targetRefresh

	for _, itemURL := range listLink {

		limitRuntime := totalTab * totalRefreshForEachPage // 35*(totalRefreshForEachPage) = 10
		totalRuntime := target / limitRuntime              // 1000 = 10

		for x := 0; x < totalRuntime; x++ {
			IDNRunningRuntime(pw, x, totalTab, totalRefreshForEachPage, itemURL)
		}

	}

	if err = pw.Stop(); err != nil {
		log.Fatalf("could not stop Playwright: %v", err)
		return err
	}

	return err
}

func IDNRunningRuntime(pw *playwright.Playwright, x, totalTab, totalRefreshForEachPage int, itemURL string) (err error) {

	fmt.Printf("Runtime %v\n", x+1)

	browser, err := pw.Chromium.Launch()
	if err != nil {
		log.Fatalf("could not launch Chromium: %v", err)
		return err
	}

	contextBrowser, err := browser.NewContext()
	if err != nil {
		log.Fatalf("could not launch Chromium: %v", err)
		return err
	}

	contextBrowser.SetDefaultNavigationTimeout(*playwright.Float(0))

	waitGroup := sync.WaitGroup{}

	for i := 0; i < totalTab; i++ {

		page, err := contextBrowser.NewPage()
		if err != nil {
			log.Fatalf("could not new page: %v", err)
		}

		waitGroup.Add(1)

		go func(page playwright.Page, tab int) {

			defer waitGroup.Done()

			_, err := page.Goto(itemURL)
			if err != nil {
				fmt.Println(err)
			}

			fmt.Println("Tab sudah terbuka mulai reload...")

			for j := 0; j < totalRefreshForEachPage; j++ {
				_, err := page.Reload(playwright.PageReloadOptions{
					WaitUntil: playwright.WaitUntilStateLoad,
				})
				if err != nil {
					fmt.Println(err)
				}
				fmt.Printf("Reload %v in Tab %v\n", j+1, tab+1)
			}

		}(page, i)

		fmt.Printf("Running Tab %v\n", i+1)

	}

	waitGroup.Wait()
	contextBrowser.ClearCookies()

	if err = contextBrowser.Close(); err != nil {
		log.Fatalf("could not close context: %v", err)
		return err
	}

	if err = browser.Close(); err != nil {
		log.Fatalf("could not close browser: %v", err)
		return err
	}

	return err

}
